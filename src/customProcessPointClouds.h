#ifndef CUSTOM_PROCESSPOINTCLOUDS_H_
#define CUSTOM_PROCESSPOINTCLOUDS_H_

#include "kdtree.h"
#include <iostream>
#include <pcl/common/common.h>
#include <set>
#include <string>
#include <unordered_set>
#include <vector>

namespace customPPC {

template <typename PointT>
std::unordered_set<int> RansacPlane(typename pcl::PointCloud<PointT>::Ptr cloud,
                                    int maxIterations, float distanceTol) {
    // Randomly sample subset and fit line

    // Measure distance between every point and fitted line
    // If distance is smaller than threshold count it as inlier

    // Return indicies of inliers from fitted line with most inliers

    std::unordered_set<int> inliersResult;
    srand(time(NULL));

    while (maxIterations--) {
        std::unordered_set<int> inliers;
        while (inliers.size() < 3) {
            inliers.insert(rand() % (cloud->points.size()));
        }
        float x1, x2, x3, y1, y2, y3, z1, z2, z3;
        PointT p1, p2, p3;
        auto itr = inliers.begin();
        p1 = cloud->points[*itr];
        itr++;
        p2 = cloud->points[*itr];
        itr++;
        p3 = cloud->points[*itr];

        float i = (p2.y - p1.y) * (p3.z - p1.z) - (p2.z - p1.z) * (p3.y - p1.y);
        float j = (p2.z - p1.z) * (p3.x - p1.x) - (p2.x - p1.x) * (p3.z - p1.z);
        float k = (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x);

        float A = i;
        float B = j;
        float C = k;
        float D = -(i * p1.x + j * p1.y + k * p1.z);
        float den = sqrt(A * A + B * B + C * C);

        for (int index = 0; index < cloud->points.size(); index++) {
            if (inliers.count(index) > 0) {
                continue;
            }

            PointT point = cloud->points[index];
            float distance =
                fabs(A * point.x + B * point.y + C * point.z + D) / den;
            if (distance <= distanceTol) {
                inliers.insert(index);
            }
        }

        if (inliers.size() > inliersResult.size()) {
            inliersResult = inliers;
        }
    }
    return inliersResult;
}

void proximity(int i, const std::vector<std::vector<float>> &points,
               std::vector<int> &cluster, KdTree *tree,
               std::vector<bool> &processed, float distanceTol);
std::vector<std::vector<int>>
euclideanCluster(const std::vector<std::vector<float>> &points, KdTree *tree,
                 float distanceTol);

} // customPPC

#endif /* CUSTOM_PROCESSPOINTCLOUDS_H_ */
