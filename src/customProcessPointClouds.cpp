#include "customProcessPointClouds.h"

namespace customPPC {

void proximity(int i, const std::vector<std::vector<float>> &points,
               std::vector<int> &cluster, KdTree *tree,
               std::vector<bool> &processed, float distanceTol) {
    processed[i] = true;
    cluster.push_back(i);
    std::vector<int> nearby = tree->search(points[i], distanceTol);
    for (auto ni : nearby) {
        if (!processed[ni]) {
            proximity(ni, points, cluster, tree, processed, distanceTol);
        }
    }
};

std::vector<std::vector<int>>
euclideanCluster(const std::vector<std::vector<float>> &points, KdTree *tree,
                 float distanceTol) {
    std::vector<bool> processed(points.size(), false);
    std::vector<std::vector<int>> clusters;
    int i = 0;
    while (i < points.size()) {
        if (processed[i]) {
            i++;
            continue;
        }

        std::vector<int> cluster;
        proximity(i, points, cluster, tree, processed, distanceTol);
        clusters.push_back(cluster);
        i++;
    }

    std::cout << "Clusters " << clusters.size() << "\n";
    return clusters;
};

} // customPPC
