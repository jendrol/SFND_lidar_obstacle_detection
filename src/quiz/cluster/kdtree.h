/* \author Aaron Brown */
// Quiz on implementing kd tree

#include "../../render/render.h"


// Structure to represent node of kd tree
struct Node
{
	std::vector<float> point;
	int id;
	Node* left;
	Node* right;

	Node(std::vector<float> arr, int setId)
	:	point(arr), id(setId), left(NULL), right(NULL)
	{}
};

struct KdTree
{
	Node* root;

	KdTree()
	: root(NULL)
	{}

	void insertHelper(Node *&node, int depth, std::vector<float> point, int id) {
		if(node == NULL)
		{
			node = new Node(point, id);
		}
		else {
			uint axis =depth % 2;
			if (point[axis] < node->point[axis]) {
				insertHelper(node->left, depth+1, point, id);
			}
			else {
				insertHelper(node->right, depth+1, point, id);
			}
		}
	}
	

	void insert(std::vector<float> point, int id)
	{
		// TODO: Fill in this function to insert a new point into the tree
		// the function should create a new node and place correctly with in the root 
		insertHelper(root, 0, point, id);

	}

	void searchHelper(Node *node, std::vector<float> target, uint depth, float distanceTol, std::vector<int>& ids) {
		//std::cout << "searchHelper point " << node->point[0] << ", " << node->point[1] << " target: " << target[0] << ", " << target[1] << "\n";
		if (node != NULL) {
			bool inBox =    fabs(target[0] - node->point[0]) < (distanceTol * 2)
				     && fabs(target[1] - node->point[1]) < (distanceTol * 2);
 		        if (inBox) {
				float xDist = target[0] - node->point[0];
				float yDist = target[1] - node->point[1];

				float distance = sqrt(xDist*xDist + yDist*yDist);
				if (distance <= distanceTol) {
					ids.push_back(node->id);
				}
			}
			    uint axis = depth % 2;
			    if (target[axis] - distanceTol < node->point[axis]) {
				    searchHelper(node->left, target, depth + 1, distanceTol, ids);
			    }
			    if (target[axis] + distanceTol > node->point[axis]) {
				    searchHelper(node->right, target, depth + 1, distanceTol, ids);
			    }
		}
	}


	// return a list of point ids in the tree that are within distance of target
	std::vector<int> search(std::vector<float> target, float distanceTol)
	{
		std::vector<int> ids;
		searchHelper(root, target, 0, distanceTol, ids);

		return ids;
	}
	

};




